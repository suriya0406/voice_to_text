from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from . models import Meeting
import re


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    password = serializers.CharField(write_only=True, required=True, validators = [validate_password])

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name','username', 'email','password')

    def validate(self, data):

        if data['first_name'] == '':
            raise serializers.ValidationError({'first name':'Please enter first name'})
        if len(data['first_name']) < 3:
            raise serializers.ValidationError({'first name':'first name is too short'})
        if not re.match(r'^[A-Za-z]+$', data['first_name']):
            raise serializers.ValidationError({'first name':'Sorry , Only letters are allowed'})
        
        if data['last_name'] == '':
            raise serializers.ValidationError({'last name':'Please enter last name'})
        if len(data['last_name']) <= 1:
            raise serializers.ValidationError({'last name':'last name is too short'})
        if not re.match(r'^[A-Za-z]+$', data['last_name']):
            raise serializers.ValidationError({'last name':'Sorry , Only letters are allowed'})
        
        if data['username'] == '':
            raise serializers.ValidationError({'user name':'Please enter user name'})
        if len(data['username']) < 5:
            raise serializers.ValidationError({'user name':'user name is too short'})
        # if not re.match(r'^[A-Za-z0-9_@.+-_]+$', data['username']):
        #     raise serializers.ValidationError({'user name':'Sorry ,  Only these @/./+/-/_ Special characters are allowed'})
        # if len(data['password']) < 6 :
        #    raise serializers.ValidationError({'password':'password length is too short, create strong password'})

        return data
        
    def create(self, validated_data):
        print(validated_data['username'])
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
            )
        user.set_password(validated_data['password'])
        user.save()
        return user

    # def update(self, instance, validated_data):
    #     instance.first_name = validated_data.get('first_name', instance.first_name)
    #     instance.last_name = validated_data.get('last_name', instance.last_name)
    #     instance.username = validated_data.get('username', instance.username)
    #     instance.email = validated_data.get('email', instance.email)
    #     instance = super().update(instance, validated_data)

    #     return instance

    def update(self, instance, validated_data):
        instance.first_name = validated_data['first_name']
        instance.last_name = validated_data['last_name']
        instance.username = validated_data['username']
        instance.email = validated_data['email']
        # instance.set_password(validated_data['password'])

        instance.save()
        return instance
    
    
        
    
class MeetingSerializer(serializers.ModelSerializer):

    class Meta:
        meeting_time = serializers.TimeField(style={
        'input_type': 'time', 'placeholder': 'time...'
        })

        model = Meeting
        fields = ['id','meeting_name','meeting_description','meeting_date','meeting_time','users_email','file_path','status']

    # def validate_meeting_name(self, meeting_name):

    #     if meeting_name == '':
    #        raise serializers.ValidationError('Please enter meeting name')

    #     elif not meeting_name.isdigit:
    #        raise serializers.ValidationError('first_name should be accepted only letters, not numbers')

    # def validate_meeting_description(self, meeting_description):

    #     if meeting_description == '':
    #        raise serializers.ValidationError('Please enter meeting name')

    #     elif meeting_description.isdigit:
    #        raise serializers.ValidationError('first_name should be accepted only letters, not numbers')

    # def validate_meeting_time(self, meeting_time):

    #     if meeting_time == '':
    #        raise serializers.ValidationError('Please enter meeting name')
    
    # def validate_users_email(self, users_email):

    #     if users_email == '':
    #        raise serializers.ValidationError('Please enter meeting name')

class UpdateUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name','username', 'email')

    def validate(self, data):

        if data['first_name'] == '':
            raise serializers.ValidationError({'first name':'Please enter first name'})
        if len(data['first_name']) < 3:
            raise serializers.ValidationError({'first name':'first name is too short'})
        if not re.match(r'^[A-Za-z]+$', data['first_name']):
            raise serializers.ValidationError({'first name':'Sorry , Only letters are allowed'})
        
        if data['last_name'] == '':
            raise serializers.ValidationError({'last name':'Please enter last name'})
        if len(data['last_name']) <= 1:
            raise serializers.ValidationError({'last name':'last name is too short'})
        if not re.match(r'^[A-Za-z]+$', data['last_name']):
            raise serializers.ValidationError({'last name':'Sorry , Only letters are allowed'})
        
        if data['username'] == '':
            raise serializers.ValidationError({'user name':'Please enter user name'})
        if len(data['username']) < 5:
            raise serializers.ValidationError({'user name':'user name is too short'})
        # if not re.match(r'^[A-Za-z0-9_@.+-_]+$', data['username']):
        #     raise serializers.ValidationError({'user name':'Sorry ,  Only these @/./+/-/_ Special characters are allowed'})
        # if len(data['password']) < 6 :
        #    raise serializers.ValidationError({'password':'password length is too short, create strong password'})

        return data