from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('create_user/', views.create_user, name="create_user"),
    path('login_user/', views.login, name="login_user"),
    path('logout_user/', views.logout_user, name="logout_user"),
    path('get_user/', views.GetUserAPI.as_view(), name="get_user"),
    path('get_all_user/', views.GetAllUserAPI.as_view(), name="get_all_user"),
    path('user_update/<int:id>', views.UserUpdateAPI.as_view(), name="user_update"),
    path('user_delete/<int:id>', views.UserDeleteAPI.as_view(), name="user_delete"),
    path('meeting_add_list/', views.MeetingAddListAPI.as_view(), name="meeting_add_list"),
    path('meeting_update/<int:id>', views.MeetingUpdateAPI.as_view(), name="meeting_update"),
    path('meeting_delete/<int:id>', views.MeetingDeleteAPI.as_view(), name="meeting_delete"),
    path('dashboard/', views.DashboardAPI.as_view(), name="dashboard"),
    path('meeting_screen_api/<int:id>/', views.MeetingScreenAPI.as_view(), name="meeting_screen_api"),
    path('download_file/<int:id>/', views.FileDownloadAPI.as_view(), name='download_file'),


]

if settings.DEBUG:
    # urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


