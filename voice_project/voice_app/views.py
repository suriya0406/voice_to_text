from django.contrib.auth.models import User
from rest_framework.response import Response
from .models import Meeting
from . serializers import MeetingSerializer, UserSerializer, UpdateUserSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.views import APIView
from django.core.mail import send_mail
from voice_project.settings import EMAIL_HOST_USER
from rest_framework.permissions import IsAuthenticated, IsAdminUser
import datetime, jwt
import os, datetime
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.http import FileResponse

@api_view(['GET', 'POST'])
def create_user(request):
    
    if request.method == 'GET':
        try:
            user = User.objects.all()
            serializer = UserSerializer(user, many=True)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif request.method == 'POST':
       
        # username= request.data['username']
        # user = User.objects.filter(username=username).exists()
        # print(user)
        # if user:
        #     raise serializers.ValidationError({'username':'Username already exists, Please try New one'})
        
        try:
            data = {
                'first_name': request.data['first_name'],
                'last_name'  : request.data['last_name'],
                'username'  : request.data['username'],
                'email'    : request.data['email'],
                'password'    :request.data['password'],
                # 'phone_number'     :request.data['phone_number']
                }  
            serializer = UserSerializer(data=data)
            if serializer.is_valid():  
                serializer.save()
                return Response(serializer.data , status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)  

@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    try:
        username = request.data.get("username")
        password = request.data.get("password")
        if username is None or password is None:
            return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if not user:
            return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
        token, _ = Token.objects.get_or_create(user=user)
        serializer = UserSerializer(user)
        return Response({
            'user_info':serializer.data, 
            'token': token.key,
            },
            status=HTTP_200_OK)
    except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)  

@api_view(["POST"])
@permission_classes([IsAuthenticated])
def logout_user(request):
    try:
        user = request.user
        print(user)
        user.auth_token.delete()
        # logout(request)
        return Response({'message':'Logged out successfully'}, status=status.HTTP_200_OK)

    except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_401_UNAUTHORIZED)  

  # @api_view(['GET'])
class GetUserAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        try:
            user = self.request.user
            print(user)
            serializer = UserSerializer(user)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetAllUserAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self,request):
        try:
            users = User.objects.all()
            serializer = UserSerializer(users, many=True)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

# @api_view(['GET', 'POST'])
class UserUpdateAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, id):
        
        try:
            user = User.objects.get(id=id)  
            serializer = UserSerializer(user)
            return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        

    def post(self, request, id):

        try:
            # user = User.objects.get(id=id)  
            # print(user.password)
            data = {
                'first_name': request.data['first_name'],
                'last_name'  : request.data['last_name'],
                'username'  : request.data['username'],
                'email'    : request.data['email'],
                # 'password'    :user.password,
                # 'phone_number'     :request.data['phone_number']
                }       
            print(data)
            # print(user) 
            user = User.objects.get(id=id)  
            # serializer = UserSerializer(user, data=data, partial = True) 
            serializer = UpdateUserSerializer(user, data=data, partial = True)
            if serializer.is_valid():  
                serializer.save()
                return Response({'message':'User details updated successfully','user_info':serializer.data} , status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# @api_view(['DELETE'])
class UserDeleteAPI(APIView):

    permission_classes = (IsAuthenticated, )
    
    def delete(self, request, id):  
        try:
            user = User.objects.get(id=id)  
            user.delete()  
            return Response({'message':'User details deleted successfully'}, status=status.HTTP_200_OK)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# @api_view(['GET', 'POST'])
class MeetingAddListAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        try:
            meeting_list = Meeting.objects.all()
            serializer = MeetingSerializer(meeting_list, many = True)
            return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):

        try:
            subject = 'Please attend the meeting.'
            message = 'Hope you are attending the meeting'  

            data = {
                'meeting_name': request.data['meeting_name'],
                'meeting_description'  : request.data['meeting_description'],
                'meeting_date'  : request.data['meeting_date'],
                'meeting_time'  : request.data['meeting_time'],
                'users_email'     :request.data['users_email']
                }   
            serializer = MeetingSerializer(data=data)
            if serializer.is_valid():  
                serializer.save()
                recepient = serializer.data['users_email']
                print(recepient)
                arr_email = []
                for rec in recepient.split(","):
                    arr_email.append(rec)
                print(arr_email)
                send_mail(subject,
                message, EMAIL_HOST_USER, arr_email, fail_silently = False)
                return Response(serializer.data , status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# @api_view(['GET', 'POST'])
class MeetingUpdateAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self, request, id):
        try:
            meeting = Meeting.objects.get(id=id)  
            serializer = MeetingSerializer(meeting)
            return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        

    def post(self, request, id):
        try:
            subject = 'Please attend the meeting.'
            message = 'Hope you are attending the meeting'  

            data = {
                'meeting_name': request.data['meeting_name'],
                'meeting_description'  : request.data['meeting_description'],
                'meeting_date'  : request.data['meeting_date'],
                'meeting_time'  : request.data['meeting_time'],
                'users_email'     :request.data['users_email']
                }   
            meeting = Meeting.objects.get(id=id)  
            serializer = MeetingSerializer(meeting,data=data)
            if serializer.is_valid():  
                serializer.save()
                recepient = serializer.data['users_email']
                print(recepient)
                arr_email = []
                for rec in recepient.split(","):
                    arr_email.append(rec)
                print(arr_email)
                send_mail(subject,
                message, EMAIL_HOST_USER, arr_email, fail_silently = False)
                return Response({'message':'Meeting details updated successfully','meeting_info':serializer.data} , status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# @api_view(['DELETE'])
class MeetingDeleteAPI(APIView):

    permission_classes = (IsAdminUser, )
    
    def delete(self, request, id):  
        try:
            meeting = Meeting.objects.get(id=id)  
            meeting.delete()  
            return Response({'message':'Meeting details deleted successfully'}, status=status.HTTP_200_OK)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class DashboardAPI(APIView):
    permission_classes = [IsAuthenticated, ]

    def get(self, request):
        try:
            today_date = datetime.date.today()
            current_time = datetime.datetime.now().strftime("%H:%M:%S")
            print(current_time)
            meeting = Meeting.objects.all()
            meeting_count = []
            file_converted = []
            today_meeting_list = []
            yet_to_start_meeting = []
            for meet in meeting:
            
                if meet.meeting_date < today_date:
                    meeting_count.append(meet)
                    if meet.file_path != '' or meet.file_path == None:
                        file_converted.append(meet.file_path)

                if meet.meeting_date == today_date:
                    data = {
                        'meeting name':meet.meeting_name,
                        'meeting description':meet.meeting_description,
                        'meeting date': meet.meeting_date,
                        'meeting time': meet. meeting_time,
                        }
                    # serializer = MeetingSerializer(meet).data
                    # today_meeting_list.append(serializer)
                    today_meeting_list.append(data)

                if meet.meeting_date >= today_date :
                    yet_to_start_meeting.append(meet)

            return Response({
                'meeting_info':{
                'Count of meetings attended':len(meeting_count),
                'Count of meetings yet to start': len(yet_to_start_meeting),
                'Count of meetings converted to voice to text': len(file_converted), 
                'Todays meeting details' : today_meeting_list
                }
                }, status=status.HTTP_200_OK)

        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class MeetingScreenAPI(APIView):

    permission_classes = [IsAuthenticated, ]
    def post(self, request,id):
        try:
            file = request.FILES['file']
            print(file)
            data = {
                'file_path':file
                }
            print(data)
            files = Meeting.objects.filter(id = id).first()
            serializer = MeetingSerializer(files, data = data, partial=True)
            # serializer.is_valid(raise_exception= True)
            # print(serializer.errors)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data , status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self,request,id):
        try:
            files = Meeting.objects.filter(id = id).first()
            file_path = files.file_path
            file=format(file_path)
            print(file)
            file_name = file.split('/')[1]
            print(file_name)
            return Response( file_name, status=status.HTTP_201_CREATED)
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class FileDownloadAPI(APIView):
    # permission_classes = [IsAuthenticated, ]

    def get(self,request, id):

        try:
            file = Meeting.objects.filter(id=id).first()
            print(file)
            file_buffer = open(file.file_path.path, "rb")
            response =FileResponse(file_buffer)
            response['Content-Type'] = 'text/plain'
            # response = Response(file_buffer, content_type=content_type)
            response['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(file.file_path.path)
            return response
        
        except Exception as error:
            return Response({"message": str(error), "success": False},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)