from django.db import models
import os

# Create your models here.

def file_path(instance, filename):
    # path = "documents/"
    format = "Text_Documents/" + filename
    return os.path.join( format)

class Meeting(models.Model):
    meeting_name = models.CharField(max_length=50)
    meeting_description = models.TextField(max_length=255)
    meeting_date = models.DateField(max_length=255)
    meeting_time = models.TimeField(blank=False)
    meeting_timezone = models.CharField(max_length = 255, default='Asia/Kolkatta')
    users_email = models.TextField(max_length=255)
    status = models.IntegerField(default=0)
    file
    class Meta:
        db_table = 'meeting'
    
    
