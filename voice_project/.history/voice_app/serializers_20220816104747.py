from curses.ascii import isdigit
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from . models import Meeting


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name','username', 'email', 'password', 'password2')

    def validate_first_name(self, first_name):

        if first_name == '':
           raise serializers.ValidationError('Please enter first name')

        elif len(first_name) < 6:
           raise serializers.ValidationError('first name is too short')

        elif first_name.isdigit:
           raise serializers.ValidationError('first name should be accepted only letters, not numbers')
    
    def validate_last_name(self, last_name):

        if last_name == '':
           raise serializers.ValidationError('please enter last name')

        elif len(last_name) < 2:
           raise serializers.ValidationError('last name is too short')

    # def validate(self, data):
    #     if data['password'] == data['password2']:
    #        raise serializers.ValidationError('Password did not match, please enter match password')
    #     return data

    
    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
            )
        user.set_password(validated_data['password'])
        user.save()
        return user

    
class MeetingSerializer(serializers.ModelSerializer):

    class Meta:
        meeting_time = serializers.TimeField(style={
        'input_type': 'time', 'placeholder': 'time...'
        })

        model = Meeting
        fields = ['id','meeting_name','meeting_description','meeting_date','meeting_time','users_email','status']

    # def validate_meeting_name(self, meeting_name):

    #     if meeting_name == '':
    #        raise serializers.ValidationError('Please enter meeting name')

    #     elif meeting_name.isdigit:
    #        raise serializers.ValidationError('first_name should be accepted only letters, not numbers')

    # def validate_meeting_description(self, meeting_description):

    #     if meeting_description == '':
    #        raise serializers.ValidationError('Please enter meeting name')

    #     elif meeting_description.isdigit:
    #        raise serializers.ValidationError('first_name should be accepted only letters, not numbers')

    # def validate_meeting_time(self, meeting_time):

    #     if meeting_time == '':
    #        raise serializers.ValidationError('Please enter meeting name')
    
    # def validate_users_email(self, users_email):

    #     if users_email == '':
    #        raise serializers.ValidationError('Please enter meeting name')