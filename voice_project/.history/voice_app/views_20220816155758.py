from turtle import update
from django.contrib.auth.models import User
from rest_framework.response import Response
from .models import Meeting
from . serializers import MeetingSerializer, UserSerializer
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.views import APIView
from django.core.mail import send_mail
from voice_project.settings import EMAIL_HOST_USER
from rest_framework.permissions import IsAuthenticated, IsAdminUser
import datetime
from rest_framework import serializers
import os, datetime

@api_view(['GET', 'POST'])
def create_user(request):
    
    if request.method == 'GET':
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)

    elif request.method == 'POST':
       
        # username= request.data['username']
        # user = User.objects.filter(username=username).exists()
        # print(user)
        # if user:
        #     raise serializers.ValidationError({'username':'Username already exists, Please try New one'})

        data = {
            'first_name': request.data['first_name'],
            'last_name'  : request.data['last_name'],
            'username'  : request.data['username'],
            'email'    : request.data['email'],
            'password'    :request.data['password'],
            'password2'    :request.data['password2'],
            # 'phone_number'     :request.data['phone_number']
        }         
        serializer = UserSerializer(data=data)
        if serializer.is_valid():  
            serializer.save()
            return Response(serializer.data , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  


# @api_view(['GET'])
class GetUserAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        user = self.request.user
        print(user)
        serializer = UserSerializer(user)
        return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)


class GetAllUserAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self,request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)


# @api_view(['GET', 'POST'])
class MeetingAddListAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self, request):
        meeting_list = Meeting.objects.all()
        serializer = MeetingSerializer(meeting_list, many = True)
        return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):

        subject = 'Please attend the meeting.'
        message = 'Hope you are attending the meeting'  

        data = {
            'meeting_name': request.data['meeting_name'],
            'meeting_description'  : request.data['meeting_description'],
            'meeting_date'  : request.data['meeting_date'],
            'meeting_time'  : request.data['meeting_time'],
            'users_email'     :request.data['users_email']
        }   

        serializer = MeetingSerializer(data=data)
        if serializer.is_valid():  
            serializer.save()
            recepient = serializer.data['users_email']
            print(recepient)
            arr_email = []
            for rec in recepient.split(","):
                arr_email.append(rec)
            print(arr_email)
            send_mail(subject,
            message, EMAIL_HOST_USER, arr_email, fail_silently = False)
            return Response(serializer.data , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET', 'POST'])
class MeetingUpdateAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self, request, id):
        meeting = Meeting.objects.get(id=id)  
        serializer = MeetingSerializer(meeting)
        return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)

    def post(self, request, id):
        subject = 'Please attend the meeting.'
        message = 'Hope you are attending the meeting'  

        data = {
            'meeting_name': request.data['meeting_name'],
            'meeting_description'  : request.data['meeting_description'],
            'meeting_date'  : request.data['meeting_date'],
            'meeting_time'  : request.data['meeting_time'],
            'users_email'     :request.data['users_email']
        }   
        meeting = Meeting.objects.get(id=id)  
        serializer = MeetingSerializer(meeting,data=data)
        if serializer.is_valid():  
            serializer.save()
            recepient = serializer.data['users_email']
            print(recepient)
            arr_email = []
            for rec in recepient.split(","):
                arr_email.append(rec)
            print(arr_email)
            send_mail(subject,
            message, EMAIL_HOST_USER, arr_email, fail_silently = False)
            return Response({'message':'Meeting details updated successfully','meeting_info':serializer.data} , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['DELETE'])
class MeetingDeleteAPI(APIView):

    permission_classes = (IsAdminUser, )
    
    def delete(self, request, id):  
        meeting = Meeting.objects.get(id=id)  
        meeting.delete()  
        return Response({'message':'Meeting details deleted successfully'}, status=status.HTTP_200_OK)


class DashboardAPI(APIView):
    permission_classes = [IsAdminUser, ]

    def post(self, request):
        time = datetime.datetime.now()
        # meeting = Meeting.objects.filter(meeting_time__gt = time)
        meeting = Meeting.objects.filter(id = 1)
        print(meeting)
        serializer = MeetingSerializer(data = meeting, many = True)
        serializer.is_valid()
        print(serializer.errors)
        print('done')

        if serializer.is_valid():
            print('jkgsdhg')
            serializer.data['status'] = 0
            serializer.save(update['status'])
        # serializer.data['status'] = 0
        # serializer.save()
        return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)


class MeetingScreenAPI(APIView):

    permission_classes = [IsAuthenticated, ]

    def post(self, request):
        date_time = datetime.datetime.now()
        file = request.FILES['file']
        file_format = format(file.name)
        # extension = os.path.splitext(file.name)[1]  # [0] returns path+filename
        file_name,extension = file_format.split('.')
        print(file_name)
        data = {
            
        }
        if extension == 'txt' or extension == 'json':
            print('success')
            files = Meeting.objects.filter(id =1)
            print(files)
            serializer = MeetingSerializer( data = files)
            print(serializer.is_valid())
            print(serializer.data['meeting_name'])

            # serializer.file_path = file_name
            
        else:
           raise serializers.ValidationError({'file':'File type is unsupported'})
    
    # def handle_uploaded_file(self, f):
    #     date_time = datetime.datetime.now()

    #     name = "static/uploads/{0}".format(f.name)
    #     file_name,extension = name.split('.')
    #     modified_name = file_name+''+date_time
    #     name = modified_name+extension
    #     with open(name, 'wb+') as destination:
    #         for chunk in f.chunks():
    #             destination.write(chunk)