from django.db import models

# Create your models here.


class Meeting(models.Model):
    meeting_name = models.CharField(max_length=50)
    meeting_description = models.TextField(max_length=255)
    meeting_time = models.TimeField(blank=False)
    meeting_timezone = models.CharField(max_length = 255, default='Asia/Kolkatta')
    users_email = models.TextField(max_length=255)
    status = models.IntegerField(default=0)
    class Meta:
        db_table = 'meeting'
    
    
