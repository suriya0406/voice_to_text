from turtle import update
from django.contrib.auth.models import User
from rest_framework.response import Response
from .models import Meeting
from . serializers import MeetingSerializer, UserSerializer
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.views import APIView
from django.core.mail import send_mail
from voice_project.settings import EMAIL_HOST_USER
from rest_framework.permissions import IsAuthenticated, IsAdminUser
import datetime
from rest_framework import serializers
import os, datetime
from django.conf import settings
from rest_framework.authtoken.models import Token
from django.contrib.auth import logout
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib import auth



@api_view(['GET', 'POST'])
def create_user(request):
    
    if request.method == 'GET':
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)

    elif request.method == 'POST':
       
        # username= request.data['username']
        # user = User.objects.filter(username=username).exists()
        # print(user)
        # if user:
        #     raise serializers.ValidationError({'username':'Username already exists, Please try New one'})

        data = {
            'first_name': request.data['first_name'],
            'last_name'  : request.data['last_name'],
            'username'  : request.data['username'],
            'email'    : request.data['email'],
            'password'    :request.data['password'],
            'password2'    :request.data['password2'],
            # 'phone_number'     :request.data['phone_number']
        }         
        serializer = UserSerializer(data=data)
        if serializer.is_valid():  
            serializer.save()
            return Response(serializer.data , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  

# class LogoutAPI(APIView):

#     def post(self, request):
#         try:
#             refresh_token = request.data.get('refresh_token')
#             print(refresh_token)
#             token = RefreshToken(refresh_token)
#             token.blacklist()

#             return Response(status=status.HTTP_200_OK)
#         except Exception as e:
#             return Response(status=status.HTTP_400_BAD_REQUEST)

class LogoutAPI(APIView):
    def post(self, request):
        try:
            user = request.user
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        logout(request)

        return Response({"success": _("Successfully logged out.")},
                    status=status.HTTP_200_OK)    

   
    # def post(self, request):
    #     token = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
    #     print(token)
    #     access_token = request.headers.get('Authorization').split(' ')[1]
    #     # access_token = authorization_heaader.split(' ')[1]
    #     print(access_token)
    #     print(self.request.user)
    #     logout(request)
    #     data = {'message': 'Sucessfully logged out'}
    #     return Response(data=data, status=status.HTTP_204_NO_CONTENT)

    # def delete(request, *args, **kwargs):
    #     id = request.session["user_id"]
    #     print(id)
    #     user = get_object_or_404(User, pk=id)
    #     auth.logout(user)
    #     # auth.logout(request)
    #     data = {
    #         "message": "You have successfully logged out.",
    #     }
    #     return Response(data, status=status.HTTP_200_OK)


    # def get(self, request, format=None):
    #     # simply delete the token to force a login
    #     user = self.request.user
    #     print(user)
    #     user_with_token = JWTTokenUserAuthentication.objects.get(user=user)
    #     print(user_with_token)
    #     user.auth_token.delete()
    #     return Response({'message':'Successfully logeed out'}, status=status.HTTP_200_OK)

# @api_view(['GET'])
class GetUserAPI(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        user = self.request.user
        print(user)
        serializer = UserSerializer(user)
        return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)


class GetAllUserAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self,request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({'user_info':serializer.data}, status=status.HTTP_200_OK)


# @api_view(['GET', 'POST'])
class MeetingAddListAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self, request):
        meeting_list = Meeting.objects.all()
        serializer = MeetingSerializer(meeting_list, many = True)
        return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)

    def post(self, request):

        subject = 'Please attend the meeting.'
        message = 'Hope you are attending the meeting'  

        data = {
            'meeting_name': request.data['meeting_name'],
            'meeting_description'  : request.data['meeting_description'],
            'meeting_date'  : request.data['meeting_date'],
            'meeting_time'  : request.data['meeting_time'],
            'users_email'     :request.data['users_email']
        }   

        serializer = MeetingSerializer(data=data)
        if serializer.is_valid():  
            serializer.save()
            recepient = serializer.data['users_email']
            print(recepient)
            arr_email = []
            for rec in recepient.split(","):
                arr_email.append(rec)
            print(arr_email)
            send_mail(subject,
            message, EMAIL_HOST_USER, arr_email, fail_silently = False)
            return Response(serializer.data , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET', 'POST'])
class MeetingUpdateAPI(APIView):

    permission_classes = (IsAdminUser, )

    def get(self, request, id):
        meeting = Meeting.objects.get(id=id)  
        serializer = MeetingSerializer(meeting)
        return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)

    def post(self, request, id):
        subject = 'Please attend the meeting.'
        message = 'Hope you are attending the meeting'  

        data = {
            'meeting_name': request.data['meeting_name'],
            'meeting_description'  : request.data['meeting_description'],
            'meeting_date'  : request.data['meeting_date'],
            'meeting_time'  : request.data['meeting_time'],
            'users_email'     :request.data['users_email']
        }   
        meeting = Meeting.objects.get(id=id)  
        serializer = MeetingSerializer(meeting,data=data)
        if serializer.is_valid():  
            serializer.save()
            recepient = serializer.data['users_email']
            print(recepient)
            arr_email = []
            for rec in recepient.split(","):
                arr_email.append(rec)
            print(arr_email)
            send_mail(subject,
            message, EMAIL_HOST_USER, arr_email, fail_silently = False)
            return Response({'message':'Meeting details updated successfully','meeting_info':serializer.data} , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['DELETE'])
class MeetingDeleteAPI(APIView):

    permission_classes = (IsAdminUser, )
    
    def delete(self, request, id):  
        meeting = Meeting.objects.get(id=id)  
        meeting.delete()  
        return Response({'message':'Meeting details deleted successfully'}, status=status.HTTP_200_OK)


class DashboardAPI(APIView):
    permission_classes = [IsAdminUser, ]

    def post(self, request):
        time = datetime.datetime.now()
        # meeting = Meeting.objects.filter(meeting_time__gt = time)
        meeting = Meeting.objects.filter(id = 1)
        print(meeting)
        serializer = MeetingSerializer(data = meeting, many = True)
        serializer.is_valid()
        print(serializer.errors)
        print('done')

        if serializer.is_valid():
            print('jkgsdhg')
            serializer.data['status'] = 0
            serializer.save(update['status'])
        # serializer.data['status'] = 0
        # serializer.save()
        return Response({'meeting_info':serializer.data}, status=status.HTTP_200_OK)

import mimetypes

class MeetingScreenAPI(APIView):

    permission_classes = [IsAuthenticated, ]

    # def post(self, request):
    #     files = Meeting.objects.filter(id = 1).first()
    #     print(files)
    #     file = request.FILES['file']
    #     file_format = format(file.name)
    #     # # extension = os.path.splitext(file.name)[1]  # [0] returns path+filename
    #     file_name,extension = file_format.split('.')
    #     # if extension == 'txt' or extension == 'json':
    #     #     files = Meeting.objects.filter(id =1)
    #     #     print(files)
    #     #     serializer = MeetingSerializer(files, data = data)
    #     #     print(serializer.is_valid())
    #     #     print(serializer.data['meeting_name'])

    #     #     # serializer.file_path = file_name
            
    #     # else:
    #     #    raise serializers.ValidationError({'file':'File type is unsupported'})
    
    def post(self, request):
        files = Meeting.objects.filter(id = 1).first()
        up_file = request.FILES['file']
        data = {
            'file_path':up_file
        }
        serializer = MeetingSerializer(files, data = data, partial=True)
        # serializer.is_valid(raise_exception= True)
        # print(serializer.errors)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data , status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 

    def get(self,request):
        files = Meeting.objects.filter(id = 1).first()
        file_path = files.file_path
        file=format(file_path)
        print(file)
        file_name = file.split('/')[1]
        print(file_name)
        return Response( file_name, status=status.HTTP_201_CREATED)

class FileDownloadAPI(APIView):
    permission_classes = [IsAuthenticated, ]

    def get(self,request, id):
        file = Meeting.objects.get(pk=id)
        file_buffer = open(file.file_path.path, "rb").read()
        content_type = 'text/plain'
        response = Response(file_buffer, content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(file.file_path.path)
        return response