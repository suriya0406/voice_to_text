from django.urls import path
from . import views
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

urlpatterns = [
    #  path('login/', LoginUserAPI.as_view(), name="login"),
    path('create_user/', views.create_user, name="create_user"),
    path('login/', TokenObtainPairView.as_view(), name='login'),
    path('refresh_token/', TokenRefreshView.as_view(), name='refresh_token'),
    path('verify_token/', TokenVerifyView.as_view(), name='verify_token'),
    path('get_user/', views.GetUserAPI.as_view(), name="get_user"),
    path('get_all_user/', views.GetAllUserAPI.as_view(), name="get_all_user"),
    path('meeting_add_list/', views.MeetingAddListAPI.as_view(), name="meeting_add_list"),
    path('meeting_update/<int:id>', views.MeetingUpdateAPI.as_view(), name="meeting_update"),
    path('meeting_delete/<int:id>', views.MeetingDeleteAPI.as_view(), name="meeting_delete"),
    path('dashboard/', views.DashboardAPI.as_view(), name="dashboard"),
    path('me/', views.DashboardAPI.as_view(), name="dashboard"),


]

