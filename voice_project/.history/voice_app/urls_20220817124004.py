from django.urls import path
from . import views
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from django.conf import settings
from django.conf.urls.static import static
from django.urls import re_path as url
from django.views.static import serve

urlpatterns = [
    #  path('login/', LoginUserAPI.as_view(), name="login"),
    path('create_user/', views.create_user, name="create_user"),
    path('login/', TokenObtainPairView.as_view(), name='login'),
    path('logout/', views.LogoutAPI.as_view(), name='logout'),
    path('refresh_token/', TokenRefreshView.as_view(), name='refresh_token'),
    path('verify_token/', TokenVerifyView.as_view(), name='verify_token'),
    path('get_user/', views.GetUserAPI.as_view(), name="get_user"),
    path('get_all_user/', views.GetAllUserAPI.as_view(), name="get_all_user"),
    path('meeting_add_list/', views.MeetingAddListAPI.as_view(), name="meeting_add_list"),
    path('meeting_update/<int:id>', views.MeetingUpdateAPI.as_view(), name="meeting_update"),
    path('meeting_delete/<int:id>', views.MeetingDeleteAPI.as_view(), name="meeting_delete"),
    path('dashboard/', views.DashboardAPI.as_view(), name="dashboard"),
    path('meeting_screen_api/', views.MeetingScreenAPI.as_view(), name="meeting_screen_api"),
    path('download_file/<int:id>/', views.download_file, name='download_file'),
    # path('download/<int:id>/', views.download_file),


]

if settings.DEBUG:
    # urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


